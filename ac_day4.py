from pprint import pprint

def get_data(file):
    with open(file, 'r') as p:
        contents = [line.replace("\n", " ").strip("\n") for line in p.read().split("\n\n")]
        data = list(filter(None, [line.strip("\n").split(" ") for line in contents]))
        data_dict = []
        for line in data:
            try:
                data_dict.append({})
                for item in line:
                    d = item.split(":")
                    data_dict[-1][d[0]] = d[1]
            except IndexError:
                pass
    return data_dict

def find_basic_valid_passports(data):
    good_passports = 0
    for dictionary in data:
        valid = True
        if 'byr' in dictionary.keys():
            if 'iyr' in dictionary.keys():
                if 'eyr' in dictionary.keys():
                    if 'ecl' in dictionary.keys():
                        if 'pid' in dictionary.keys():
                            if 'hcl' in dictionary.keys():
                                if 'hgt' in dictionary.keys():
                                    good_passports += 1                       
    return good_passports

def find_fully_valid_passports(data):
    good_passports = 0
    for dictionary in data:
        valid = True
        if 'byr' in dictionary.keys() and 2002 >= int(dictionary["byr"]) >= 1920 and len(dictionary["byr"])==4:
            if 'iyr' in dictionary.keys() and 2020 >= int(dictionary["iyr"]) >= 2010 and len(dictionary["iyr"])==4:
                if 'eyr' in dictionary.keys() and 2030 >= int(dictionary["eyr"]) >= 2020 and len(dictionary["eyr"])==4:
                    if 'ecl' in dictionary.keys() and dictionary["ecl"] in eye_colours:
                        if 'pid' in dictionary.keys() and dictionary["pid"].isdigit() and len(dictionary["pid"])==9:
                            if 'hcl' in dictionary.keys() and len(dictionary["hcl"])==7 and all(num in valid_char for num in dictionary["hcl"]):
                                if 'hgt' in dictionary.keys():
                                    if dictionary["hgt"][-2:] == "cm":
                                        try:
                                            if 193 >= int(dictionary["hgt"][0:3]) >= 150:
                                                good_passports += 1
                                        except ValueError:
                                            pass
                                    elif dictionary["hgt"][-2:] == "in":
                                        if 76 >= int(dictionary["hgt"][0:2]) >= 59:
                                              good_passports += 1                       
    return good_passports            

def ac41(path):
    data = get_data(path)
    return find_basic_valid_passports(data)

def ac42(path):
    data = get_data(path)
    return find_fully_valid_passports(data)

valid_char = "#0123456789abcdef"
eye_colours = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]    
fields = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']
path = r"day4.txt"
print(f"The Answer to part 1 is {ac41(path)}")
print(f"The Answer to part 2 is {ac42(path)}")
def unique(dset):
    return len(list(dict.fromkeys([n for n in dset])))

def everyone(dset):
    common = [list(dict.fromkeys([n for n in line])) for line in dset]
    return len(list(set.intersection(*map(set, [dset for dset in common]))))

def ac61(path):
    data = [line.replace("\n", " ").strip("\n") for line in open(path, 'r').read().split("\n\n")]
    return sum(unique(dset.replace(" ", "")) for dset in data)

def ac62(path):
    dset = [line.replace("\n", " ").strip("\n") for line in open(path, 'r').read().split("\n\n")]
    data = [line.split() for line in dset]
    return sum(everyone(dset) for dset in data)

path = r"day6.txt"
print(f"The Answer to part 1 is {ac61(path)}")
print(f"The Answer to part 2 is {ac62(path)}")
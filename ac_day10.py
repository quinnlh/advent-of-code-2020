from itertools import permutations
from numpy import prod
from math import factorial

def check_next_3(data, current):
    if current+1 in data:
        data.remove(current+1)
        return data, current+1, 'ones'
    elif current+2 in data:
        data.remove(current+2)
        return data, current+2, 'twos'
    elif current+3 in data:
        data.remove(current+3)
        return data, current+3, 'threes'
    else:
        return False

def parse_plugs(data):
    n, ones, twos, threes = 0, 0, 0, 0
    while len(data)>0:
        try:
            data, n, count = check_next_3(data, n)
            if count=='ones':
                ones += 1
            elif count =='threes':
                threes += 1
            elif count =='twos':
                twos += 1
        except TypeError:
            pass
    return ones*(threes+1)

def is_valid(set):
    return len([set[n] for n in range(len(set)-1) if set[n+1]<=set[n]+3])==len(set)-1

def isvalid2(data):
    count = 0
    for n in range(len(data)):
        try:
            count += len([perm for perm in permutations(data, n) if is_valid(perm)])
        except MemoryError:
            print('memory crapped out')
    return count

def find_combos(data):
    choices = []
    loops_evaluated = 0
    for n in range(len(data)-3):
        if data[n+3]<=data[n]+3:
            #val_post_n = find_combos(data[n+3:])
            choices.append(3)
            loops_evaluated+=1
        elif data[n+2]<=data[n]+3:
            #val_post_n = find_combos(data[n+2:])
            loops_evaluated+=1
            choices.append(2)
        elif data[n+1]<=data[n]+3:
            #val_post_n = find_combos(data[n+1:])
            choices.append(1)
            loops_evaluated+=1
        #print(f"val_post_n: {val_post_n}\nloops evaluated: {loops_evaluated}")
    print(choices)
    #print(f"ones: {choices.count(1)}")
    return prod(choices)

def ac91():
    data = [int(line) for line in open(r'day10.txt', 'r').readlines()]
    return parse_plugs(data)

def ac92():
    data = sorted([int(line) for line in open(r'day10-test.txt', 'r').readlines()])
    #data.append(data[-1]+3)
    #print(isvalid2(data))
    return find_combos(data)

print(f"Answer to part 1 is: {ac91()} \nAnswer to part 2 is: {ac92()}")
#wrong: 1632,
#wrong2: 9043968, 18087936

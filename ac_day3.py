def get_data(file):
    with open(file, 'r') as p:
        contents = p.read()
        data = list(filter(None, contents.split("\n")))
    p.close()
    return data

def find_trees(data, x, y):
    trees = 0
    for n in range(len(data)):
        row = n*x
        column = n*y %31
        try:
            if data[row][column] == "#": 
                trees +=1
        except IndexError:
            pass
    return trees

def ac31(path):
    data = get_data(path)
    answer = find_trees(data, 1, 3)
    return answer

def ac32(path):
    data = get_data(path)
    total_trees = 1
    for y, x in [[1, 1], [3, 1], [5, 1], [7, 1], [1, 2]]:
        trees = find_trees(data, x, y)
        total_trees *= trees
    return total_trees

path = r"day3.txt"
print(f"The Answer to part 1 is {ac31(path)}")
print(f"The Answer to part 2 is {ac32(path)}")

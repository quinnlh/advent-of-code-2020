class bagchecker:
    def __init__(self, path):
        self.path = path
        self.untested = []
        self.invalid = []
        self.valid = []
        self.parse_bags()
        self.bag_list = []
        self.total_bags = 0

    def parse_bags(self):
        data = [line.strip(".\n") for line in open(self.path, 'r').readlines()]
        self.bags = {}
        for line in data:
            bag = line.strip(",").split()
            clr, contents = f"{bag[0]} {bag[1]}", self.get_contents(bag[2:])
            if not contents:
                self.invalid.append(clr)
            else:
                self.bags[clr] = contents
                self.untested.append(clr)

    def get_contents(self, baglist):
        contents = []
        remove = ['bag', 'bags', 'contain']
        bag = [word for word in baglist if word.strip(',') not in remove]
        for n in range(0, len(bag), 3):
            try:
                contents.append([bag[n], f"{bag[n+1]} {bag[n+2]}"])
            except IndexError:
                contents = False
        return contents

    def bag_check(self, colour):
        try:
            for key in self.bags.keys():
                for n in range(len(self.bags[key])):
                    if colour in self.bags[key][n]:
                        if key not in self.valid:
                            self.valid.append(key)
                        if key in self.untested:
                            self.untested.remove(key)
        except TypeError:
            pass

    def count_sub_bags(self, colour, numbags):
        try:
            for bag in self.bags[colour]:
                if bag is not None:
                    self.total_bags += numbags*int(bag[0])
                    self.count_sub_bags(bag[1], numbags*int(bag[0]))
        except TypeError:
            pass
        except KeyError:
            pass

    def loop_through_bags(self):
        keep_going = True
        while len(self.untested) > 0 and keep_going:
            valid_so_far = len(self.valid)
            for colour in self.valid:
                self.bag_check(colour)
            keep_going = len(self.valid) != valid_so_far
    
    def check_for_bag(self, colour):
        self.valid.append(colour)
        self.bag_check(colour)
        self.loop_through_bags()
        self.valid.remove(colour)
        return len(self.valid)

def ac71(path):
    bagger = bagchecker(path)
    hold_gold = bagger.check_for_bag('shiny gold')
    return hold_gold

def ac62(path):
    bagger = bagchecker(path)
    bagger.count_sub_bags('shiny gold', 1)
    return bagger.total_bags


path = r"day7.txt"
print(f"The Answer to part 1 is {ac71(path)}")
print(f"The Answer to part 2 is {ac62(path)}")

def match2(data, value):
    for n in range(len(data)-1):
        for m in range(len(data)-1):
            if int(data[n])+int(data[m])==value:
                return int(data[n]), int(data[m])
                
def match3(data, value):
    for n in range(len(data)-1):
        for m in range(len(data)-1):
            for l in range(len(data)-1):
                if int(data[n])+int(data[m])+int(data[l])==value:
                    return int(data[n]), int(data[m]), int(data[l])

def get_list(file):
    with open(file, 'r') as p:
        data = p.read().split("\n")
    p.close()
    return data

def ac11(path):
    data = get_list(path)
    n1, n2 = match2(data, 2020)
    return n1*n2

def ac12(path):
    data = get_list(path)
    n1, n2, n3 = match3(data, 2020)
    return n1*n2*n3

path = r"day1.txt"  
print(ac12(path))
def rotate(facing, direction, degree):
    directions = ['N', 'E', 'S', 'W', 'N', 'E', 'S', 'W']
    position = int(degree / 90)
    if direction == 'L':
        return directions[directions.index(facing, 4) - position]
    elif direction == 'R':
        return directions[directions.index(facing) + position]
    else:
        print("Invalid rotation")

def move_cardinal(direction, facing, steps, pos):
    if direction=='N':
        pos[0] += steps
    if direction=='S':
        pos[0] -= steps
    if direction=='E':
        pos[1] += steps
    if direction=='W':
        pos[1] -= steps
    return pos

def move_dir(facing, steps, pos):
    if facing=='N':
        pos[0] += steps
    if facing=='S':
        pos[0] -= steps
    if facing=='E':
        pos[1] += steps
    if facing=='W':
        pos[1] -= steps
    return pos

def proceed(data):
    pos = [0, 0]
    facing = 'E'
    cardinal = ['N', 'S', 'E', 'W']
    LR = ['L', 'R']
    for n in range(len(data)):
        direction = data[n][0]
        steps = data[n][1]
        if direction in cardinal:
            pos = move_cardinal(direction, facing, steps, pos)
        elif direction in LR:
            facing = rotate(facing, direction, steps)
        elif direction == 'F':
            pos = move_dir(facing, steps, pos)
        else:
            print('Invalid instruction')
    return pos


def ac_121(path):
    data = [(line.strip()[0], int(line.strip()[1:])) for line in open(path, 'r').readlines()]
    return sum(proceed(data))
    #print(data)



print(ac_121(r'day12.txt'))


#wrong: 




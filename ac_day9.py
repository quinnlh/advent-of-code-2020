def checksum(data, val, currentn, nbefore):
    for n in range(currentn-nbefore, currentn):
        for m in range(currentn-nbefore, currentn):
            if m != n:
                if data[n]+data[m] == val:
                    return True

def find_invalid_entry(data, nbefore):
    for n in range(nbefore, len(data)):
        if not checksum(data, data[n], n, nbefore):
            return data[n]

def get_sum_of_entries(data, nentries, value):
    for n in range(2, len(data)-nentries):
        if sum(data[n:n+nentries]) == value:
            return data[n:n+nentries]

def find_contiguous_sum(data, value):
    for n in range(2, len(data)):
        checklist = get_sum_of_entries(data, n, value)
        if checklist:
            return max(checklist)+min(checklist)

def ac91():
    data = [int(line) for line in open(r'day9.txt', 'r').readlines()]
    return find_invalid_entry(data, 25)

def ac92():
    data = [int(line) for line in open(r'day9.txt', 'r').readlines()]
    return find_contiguous_sum(data, find_invalid_entry(data, 25))

print(f"Answer to part 1 is: {ac91()} \nAnswer to part 2 is: {ac92()}")
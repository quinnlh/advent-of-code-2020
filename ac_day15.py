def numcheck(number, gamebag, count):
    if number in gamebag.keys():
        lst = gamebag[number]
        lst.append(count)
        gamebag[number] = lst
        return gamebag[number][-1]-gamebag[number][-2]
    else:
        gamebag[number] = [count]
        return 0

def play_game(num_in, stopval):
    count = 1
    gamebag = {}
    for n in range(len(num_in)-1):
        num = num_in[n]
        gamebag[num] = [count]
        count += 1
        lastnum = num_in[n+1]

    while count < stopval:
        currentnum = numcheck(lastnum, gamebag, count)
        count += 1
        lastnum = currentnum
    return lastnum

start_num = [0,14,1,3,7,9]
print(f"The Answer to part 1 is {play_game(start_num, 2020)}")
print(f"The Answer to part 2 is {play_game(start_num, 30000000)}")
def apply_mask(valstring, mask):
    for i in range(36):
        if mask[i] == '0':
            valstring = valstring[:i]+'0'+valstring[i+1:]
        elif mask[i] == '1':
            valstring = valstring[:i]+'1'+valstring[i+1:]
    return valstring


def run_instruction(bitmask, value, location, memory):
    #bitmask, value, and memory in string format
    valstring = str(format(int(value), '010b'))
    while len(valstring) < len(bitmask):
        valstring = '0' + valstring
    masked_valstring = apply_mask(valstring, bitmask)
    return update_memory(location, masked_valstring, memory)

def update_memory(location, value, memory):
    memory[location] = int(value, 2)
    return memory

def update_mask(raw_mask):
    return raw_mask.split('=')[1].strip()

def parse_instruction(raw_instr):
    return raw_instr.split('=')[1].strip(), raw_instr.split('=')[0].strip('mem[ ]')

def run_bitmask(data):
    memory = {}
    for line in data:
        if line[:4] == 'mask':
            mask = update_mask(line)
        else:
            value, location = parse_instruction(line)
            memory = run_instruction(mask, value, location, memory)
    return memory

def sum_memory(memory):
    return sum([memory[key] for key in memory.keys()])

def ac141(path):
    #return format(53200, '010b')
    data = [line.strip('\n') for line in open(path, 'r').readlines()]
    return sum_memory(run_bitmask(data))
    #mask = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X'
    #print(run_instruction(mask, 11, 8, {}))

def ac142(path):
    bagger = bagchecker(path)
    bagger.count_sub_bags('shiny gold', 1)
    return bagger.total_bags


print(f"The Answer to part 1 is {ac141(r'day14.txt')}")
#print(f"The Answer to part 2 is {ac142(r"day14.txt")}")
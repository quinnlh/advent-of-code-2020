from time import perf_counter
import copy
def jmp(data, n, accumulator):
    return data, n+int(data[n][1]), accumulator

def nop(data, n, accumulator):
    return data, n+1, accumulator

def acc(data, n, accumulator):
    accumulator += int(data[n][1])
    return data, n+1, accumulator

def run_game_once(data):
    n, accumulator, been_run = 0, 0, []
    while n < len(data):
        if n in been_run:
            return accumulator
        been_run.append(n)
        func = data[n][0]
        argument = data[n][1]
        data, n, accumulator = inst[func](data, n, accumulator)

def run_game(data):
    wait, start = .0002, perf_counter()
    n, accumulator = 0, 0
    while n < len(data):
        if perf_counter() > start+wait:
            return False
        func = data[n][0]
        argument = data[n][1]
        data, n, accumulator = inst[func](data, n, accumulator)
    return accumulator

def find_issue_cmd(data):
    for n in range(len(data)):
        iterdata = copy.deepcopy(data)
        if iterdata[n][0] == 'jmp':
            iterdata[n][0] = 'nop'
        elif iterdata[n][0] == 'nop':
            iterdata[n][0] = 'jmp'
        ans = run_game(iterdata)
        if ans:
            return ans

def ac81(path):
    data = []
    for line in open(path, 'r').readlines():
        if '+' in line:
            line =line.replace(' +', "")
        line = line.replace('\n', " ")
        line.strip()
        data.append([line[:3], line[3:].strip()])
    return run_game_once(data)

def ac82(path):
    data = []
    for line in open(path, 'r').readlines():
        if '+' in line:
            line =line.replace(' +', "")
        line = line.replace('\n', " ")
        line.strip()
        data.append([line[:3], line[3:].strip()])
    return find_issue_cmd(data)

def main():
    global inst
    inst = {
        'acc': acc,
        'jmp': jmp,
        'nop': nop
    }
    path = r"day8.txt"
    print(f"The Answer to part 1 is {ac81(path)}")
    print(f"The Answer to part 2 is {ac82(path)}")

if __name__ == "__main__":
    main()
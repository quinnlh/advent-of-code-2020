def find_bus(data):
    bus_wait = [bus - data[0]%bus for bus in data[1]]
    return data[1][bus_wait.index(min(bus_wait))] * min(bus_wait)

def find_sequential_timestamp(data):
    count = 0
    step = 1
    for point in data:
        while True:
            if (point[1] + count)%point[0] == 0:
                break
            else:
                count += step
        step *= point[0]
    return count

def ac_131(path):
    rawdata = [bus.strip('\n').split(',') for bus in open(path, 'r').readlines()]
    data = [int(rawdata[0][0]), [int(bus) for bus in rawdata[1] if bus.isnumeric()]]
    return find_bus(data)

def ac_132(path):
    rawdata = [bus.strip('\n').split(',') for bus in open(path, 'r').readlines()]
    data = [int(bus) if bus.isnumeric() else bus  for bus in rawdata[1]]
    dset = [[data, n] for n, data in enumerate(data) if type(data) == int]
    return find_sequential_timestamp(dset)

print(f"Answer to part 1 is: {ac_131(r'day13.txt')}")
print(f"Answer to part 2 is: {ac_132(r'day13.txt')}")
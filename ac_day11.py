from pprint import pprint
def check_neighbors(data, n, m, adjacent, maxneighbor):
	if data[n][m] == 'L' and '#' not in adjacent:
		return '#'
	elif data[n][m] == '#' and adjacent.count("#") >= maxneighbor:
		return 'L'
	else:
		return data[n][m]

def apply_rules(data, maxneighbor):
	updated = []
	
	for n in range(len(data)):
		row = []
		for m in range(len(data[n])):
			maxrow = n == len(data)-1
			maxcol = m == len(data[n])-1
			adj = get_adj(data, n, m, maxrow, maxcol)
			row.append(check_neighbors(data, n, m, adj, maxneighbor))
		updated.append("".join(row))
	return updated

def get_adj(data, n, m, maxrow, maxcol):
	#print(f"debug {n, m}")
	#print(f"length of row, column: {len(data), len(data[n])}")
#	adj = [
#	    0    data[n-1][m-1], 0 1 2  
#	    1    data[n-1][m],   3   4
#	    2    data[n-1][m+1], 5 6 7
#	    3    data[n][m-1],
#	    4    data[n][m+1],
#	    5    data[n+1][m-1],
#	    6    data[n+1][m],
#	    7    data[n+1][m+1]
#	        ]
	if n == 0:
		if m == 0:
			#top left
			return [data[n+1][m-1], data[n+1][m], data[n+1][m+1]]
		elif maxcol:
			#top right
			return [data[n][m-1], data[n+1][m-1], data[n+1][m]]
		else:
			#top row
			return [data[n][m-1], data[n][m+1],data[n+1][m-1],data[n+1][m],data[n+1][m+1]]

	elif maxrow:
		if m == 0:
			#bottom left
			return [data[n-1][m], data[n-1][m+1], data[n][m+1]]
		elif maxcol:
			#bottom right
			return [data[n-1][m-1], data[n-1][m], data[n][m-1]]
		else:
			#bottom row
			return [data[n-1][m-1], data[n-1][m], data[n-1][m+1], data[n][m-1], data[n][m+1]]

	elif m == 0:
		#left side
		return [data[n-1][m], data[n-1][m+1], data[n][m+1], data[n+1][m], data[n+1][m+1]]

	elif maxcol:
		#right side
		return [data[n-1][m-1], data[n-1][m], data[n][m-1], data[n+1][m-1], data[n+1][m]]
	else:
		#the entire centre
		return [
	        data[n-1][m-1], 
	        data[n-1][m],   
	        data[n-1][m+1], 
	        data[n][m-1],
	        data[n][m+1],
	        data[n+1][m-1],
	        data[n+1][m],
	        data[n+1][m+1]
	        ]

def get_visible(data, n, m, maxrow, maxcol):
	#print(f"debug {n, m}")
	#print(f"length of row, column: {len(data), len(data[n])}")
#	adj = [
#	    0    data[n-1][m-1], 0 1 2  
#	    1    data[n-1][m],   3   4
#	    2    data[n-1][m+1], 5 6 7
#	    3    data[n][m-1],
#	    4    data[n][m+1],
#	    5    data[n+1][m-1],
#	    6    data[n+1][m],
#	    7    data[n+1][m+1]
#	        ]
	if n == 0:
		if m == 0:
			#top left
			return [data[n+1][m-1], data[n+1][m], data[n+1][m+1]]
		elif maxcol:
			#top right
			return [data[n][m-1], data[n+1][m-1], data[n+1][m]]
		else:
			#top row
			return [data[n][m-1], data[n][m+1],data[n+1][m-1],data[n+1][m],data[n+1][m+1]]

	elif maxrow:
		if m == 0:
			#bottom left
			return [data[n-1][m], data[n-1][m+1], data[n][m+1]]
		elif maxcol:
			#bottom right
			return [data[n-1][m-1], data[n-1][m], data[n][m-1]]
		else:
			#bottom row
			return [data[n-1][m-1], data[n-1][m], data[n-1][m+1], data[n][m-1], data[n][m+1]]

	elif m == 0:
		#left side
		return [data[n-1][m], data[n-1][m+1], data[n][m+1], data[n+1][m], data[n+1][m+1]]

	elif maxcol:
		#right side
		return [data[n-1][m-1], data[n-1][m], data[n][m-1], data[n+1][m-1], data[n+1][m]]
	else:
		#the entire centre
		return [
	        data[n-1][m-1], 
	        data[n-1][m],   
	        data[n-1][m+1], 
	        data[n][m-1],
	        data[n][m+1],
	        data[n+1][m-1],
	        data[n+1][m],
	        data[n+1][m+1]
	        ]

def run1(data):
	for n in range(100):
		data = apply_rules(data, 4)
	return data


def ac_111(path):
    data = [line.strip() for line in open(path, 'r').readlines()]
    data = run1(data)
    return sum([data[n].count('#') for n in range(len(data))])
    #print(data)



print(ac_111(r'day11.txt'))


#wrong: 1029, 




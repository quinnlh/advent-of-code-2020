def countem(data, value):
    count = 0
    for n in range(len(data)):
        if data[n] == value:
            count += 1
    return count

def get_list(file):
    with open(file, 'r') as p:
        contents = p.read()
        data = contents.split("\n")
    p.close()
    return data

def ac21(path):
    data = get_list(path)
    valid_pass = 0
    for n in range(len(data)):
        if data[n] != "":
            subset = data[n].split(" ")
            count = int(countem(subset[2], subset[1][0]))
            lower_bound = int(subset[0].split("-")[0])
            upper_bound = int(subset[0].split("-")[1])

            if (count >= lower_bound) and (count <= upper_bound):
                valid_pass +=1
    return valid_pass

def ac22(path):
    data = get_list(path)
    valid_pass = 0
    for n in range(len(data)):
        if data[n] != "":
            subset = data[n].split(" ")
            min_index = int(subset[0].split("-")[0])
            max_index = int(subset[0].split("-")[1])
            letter = subset[1][0]
            dset = subset[2]

            if dset[min_index-1] == letter:
                if dset[max_index-1] == letter:
                    pass
                else:
                    valid_pass +=1
            elif dset[max_index-1] == letter:
                valid_pass +=1
            else:
                pass

    return valid_pass

path = r"day2.txt"  
print(f"The Answer is {ac22(path)}")



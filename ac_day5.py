def get_data(file):
    with open(file, 'r') as p:
        contents = p.read()
        data = list(filter(None, contents.split("\n")))
    p.close()
    return data

def find_row(databite, max_row):
    min_row = 0
    databite = databite[:7]
    for n in range(len(databite)):
        if databite[n] == 'F':
            max_row = (max_row+min_row) //2
            last = "max"
        elif databite[n] == 'B':
            min_row += (max_row-min_row) //2
            last = "min"
    if last == "min":
        return min_row
    elif last == "max":
        return max_row-1

def find_column(databite, max_column):
    min_column = 0
    databite = databite[-3:]
    for n in range(len(databite)):
        if databite[n] == 'L':
            max_column = (max_column+min_column) //2
            last = "max"
        elif databite[n] == 'R':
            min_column += (max_column-min_column) //2
            last = "min"
    if last == "min":
        return min_column
    elif last == "max":
        return max_column-1

def find_seat(databite):
    row = find_row(databite, 128)
    column = find_column(databite, 8)
    return row, column
    
def ac51(path):
    data = get_data(path)
    seats = []
    for line in data:
        row, column = find_seat(line)
        seats.append(row*8 + column)
    return max(seats)

def ac52(path):
    data = get_data(path)
    seats = []
    empty_seats = []
    for line in data:
        row, column = find_seat(line)
        seats.append(row*8 + column)
        #seats.append([row, column])
    for n in range(len(seats)):
        if n not in seats:
            empty_seats.append(n)
    for seat in empty_seats:
        if seat +1 in empty_seats or seat-1 in empty_seats:
            pass
        else:
            return seat

path = r"day5.txt"
print(f"The Answer to part 1 is {ac51(path)}")
print(f"The Answer to part 2 is {ac52(path)}")
